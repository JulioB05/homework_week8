This application allows you to view the Marvel api, it has an implementation in vercel:
https://homework-week8.vercel.app/

You can find the following pages:

- Characters
- Comics
- Stories
- Bookmark

You can also see in detail the information of each element shown on the pages
