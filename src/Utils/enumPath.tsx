import React from 'react';

enum PathRoutes {
  Home = '/',
  Details = '/Details',
  Login = '/login',
  SignUp = '/signup',
  Comics = '/comics',
  Characters = '/characters',
  Stories = '/stories',
  Bookmarks = '/bookmarks',
}

export default PathRoutes;
