const FiltersFormat = {
  type: 'format',
  data: [
    { id: 'comic', name: 'comic' },
    { id: 'magazine', name: 'magazine' },
    { id: ' trade paperback', name: 'trade paperback' },
    { id: 'hardcover', name: 'hardcover' },
    { id: 'digest', name: 'digest' },
    { id: 'graphic novel', name: 'graphic novel' },
    { id: 'digital comic', name: 'digital comic' },
    { id: 'infinite comic', name: 'infinite comic' },
  ],
};

const FiltersTitle = {
  type: 'title',
  data: [
    { id: 'Marvel', name: 'Marvel' },
    { id: 'X-men', name: 'X-men' },
    { id: 'Ant-Man', name: 'Ant-Man' },
    { id: 'Spider-Man', name: 'Spider-Man' },
  ],
};
export { FiltersFormat, FiltersTitle };
