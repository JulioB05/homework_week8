const FilterCharacter = {
  type: 'characters',
  data: [
    { id: 1009163, name: 'Aurora' },
    { id: 1009164, name: 'Avalanche' },
    { id: 1009165, name: 'Avengers' },
    { id: 1011766, name: 'Azazel' },
    { id: 1016452, name: 'Spiderman' },
    { id: 1011010, name: 'Spiderman Ultimate' },
    { id: 1010888, name: 'Iron Fist' },
    { id: 1017294, name: 'Lego Iron Man' },
    { id: 1009487, name: 'Iron Patriot' },
  ],
};

export default FilterCharacter;
