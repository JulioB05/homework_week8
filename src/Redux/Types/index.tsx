//apiReducerCharacters
export const readAllDataCharacter = 'readAllDataCharacter';
export const readFiltersSelectCharacters = 'readFiltersSelectCharacters';
export const readCurrentPageCharacters = 'readCurrentPageCharacters';

//apiReducerComics
export const readAllDataComics = 'readAllDataComics';
export const readSizeOfDataComics = 'readSizeOfDataComics';
export const pageComics = 'pageComics';
export const noDataComics = 'noDataComics';
//apiReducerStories
export const readAllDataStories = 'readAllDataStories';

//Bookmarks
export const addToBookmark = 'addToBookmark';
export const removeOneBookmark = 'removeOneBookmark';
export const removeAllBookmarks = 'removeAllBookmarks';
export const clearBookmark = 'clearBookmark';
export const readAllBookmarks = 'readAllBookmarks';
