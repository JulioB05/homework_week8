import { combineReducers } from 'redux';
import apiReducerCharacter from './apiReducerCharacter';
import apiReducerComics from './apiReducerComics';
import apiReducerStories from './apiReducerStories';
import bookmarksReducer from './bookmarkReducer';

const reducer = combineReducers({
  characterReducer: apiReducerCharacter,
  comicsReducer: apiReducerComics,
  storiesReducer: apiReducerStories,
  bookmarks: bookmarksReducer,
});

export default reducer;
