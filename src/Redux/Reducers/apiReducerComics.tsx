import { Data } from '../../Interfaces/apiComics';
import { actionTypes } from '../Actions/apiComicsActions';
import { pageComics, readAllDataComics, readSizeOfDataComics } from '../Types';

interface apiInitialStateInterface {
  dataBaseComics?: Data;
  numberComics: number;
  page: number;
}

export const apiInitialState: apiInitialStateInterface = {
  numberComics: 0,
  page: 1,
};

const apiReducerComics = (state = apiInitialState, action: actionTypes) => {
  switch (action.type) {
    case readAllDataComics: {
      return {
        ...state,
        dataBaseComics: action.payload,
      };
    }
    case readSizeOfDataComics: {
      return {
        ...state,
        numberComics: action.payload,
      };
    }
    case pageComics: {
      return {
        ...state,
        page: action.payload,
      };
    }

    default:
      return state;
  }
};
export default apiReducerComics;
