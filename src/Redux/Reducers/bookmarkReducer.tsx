import { actionBookmarkTypes } from '../Actions/bookmarkActions';
import {
  addToBookmark,
  removeAllBookmarks,
  removeOneBookmark,
  clearBookmark,
  readAllBookmarks,
} from '../Types';

interface bookmarkObj {
  id: number;
  name: string;
  type: string;
  img: string;
}
export interface bookmarksType {
  bookmarks: bookmarkObj[];
}

export const initialState: bookmarksType = {
  bookmarks: [],
};

const bookmarksReducer = (
  state = initialState,
  action: actionBookmarkTypes
) => {
  switch (action.type) {
    case addToBookmark: {
      return {
        ...state,
        bookmarks: [...state.bookmarks, action.payload],
      };
    }

    case removeOneBookmark: {
      let newData = state.bookmarks.filter((el) => el.id !== action.payload);

      return {
        ...state,
        bookmarks: newData,
      };
    }
    case removeAllBookmarks: {
      return {
        ...state,
        bookmarks: [],
      };
    }

    case clearBookmark:
      return initialState;
    default:
      return state;
  }
};

export default bookmarksReducer;
