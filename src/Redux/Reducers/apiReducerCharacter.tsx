import { Data, Result } from '../../Interfaces/apiCharacters';
import { actionTypes } from '../Actions/apiCharacterActions';
import {
  readAllDataCharacter,
  readFiltersSelectCharacters,
  readCurrentPageCharacters,
} from '../Types';

interface apiInitialStateInterface {
  dataBaseCharacter?: Data;
  filters?: { type: string; id: string };
  page: number;
}

export const apiInitialState: apiInitialStateInterface = {
  page: 1,
};

const apiReducerCharacter = (state = apiInitialState, action: actionTypes) => {
  switch (action.type) {
    case readAllDataCharacter: {
      return {
        ...state,
        dataBaseCharacter: action.payload,
      };
    }
    case readFiltersSelectCharacters: {
      return {
        ...state,
        filters: action.payload,
      };
    }
    case readCurrentPageCharacters: {
      return {
        ...state,
        page: action.payload,
      };
    }

    default:
      return state;
  }
};
export default apiReducerCharacter;
