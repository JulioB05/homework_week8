import { Data } from '../../Interfaces/apiStories';
import { actionTypes } from '../Actions/apiStoriesActions';
import { readAllDataStories } from '../Types';

interface apiInitialStateInterface {
  dataBaseStories?: Data;
}

export const apiInitialState: apiInitialStateInterface = {};

const apiReducerStories = (state = apiInitialState, action: actionTypes) => {
  switch (action.type) {
    case readAllDataStories: {
      return {
        ...state,
        dataBaseStories: action.payload,
      };
    }

    default:
      return state;
  }
};
export default apiReducerStories;
