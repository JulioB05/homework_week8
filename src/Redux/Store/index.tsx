import { legacy_createStore as createStore } from 'redux';
import reducer from '../Reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
};

const persistReducerApp = persistReducer(persistConfig, reducer);
const store = createStore(persistReducerApp);

const persistor = persistStore(store);

//store.subscribe(() => console.log(store));

export { store, persistor };
