import { Data } from '../../Interfaces/apiStories';
import { readAllDataStories } from '../Types';

export type actionTypes = { type: 'readAllDataStories'; payload: Data };

export const saveAllDataStories = (data: Data) => ({
  type: readAllDataStories,
  payload: data,
});
