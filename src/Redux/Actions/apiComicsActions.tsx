import { Data, Result } from '../../Interfaces/apiComics';
import { readAllDataComics, readSizeOfDataComics, pageComics } from '../Types';

export type actionTypes =
  | { type: 'readAllDataComics'; payload: Data }
  | { type: 'readSizeOfDataComics'; payload: number }
  | { type: 'pageComics'; payload: number };

export const saveAllDataComics = (data: Data) => ({
  type: readAllDataComics,
  payload: data,
});
export const sizeOfDataComics = (data: number) => ({
  type: readSizeOfDataComics,
  payload: data,
});
export const savePageComics = (data: number) => ({
  type: pageComics,
  payload: data,
});
