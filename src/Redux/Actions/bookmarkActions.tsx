import {
  addToBookmark,
  removeAllBookmarks,
  removeOneBookmark,
  clearBookmark,
  readAllBookmarks,
} from '../Types';

export type actionBookmarkTypes =
  | { type: 'addToBookmark'; payload: number }
  | {
      type: 'removeAllBookmarks';
      payload: number;
    }
  | { type: 'removeOneBookmark'; payload: number }
  | { type: 'clearBookmark' }
  | { type: 'readAllBookmarks' };

export const addBookmark = (data: {
  id: number;
  name: string;
  type: string;
  img: string;
}) => ({
  type: addToBookmark,
  payload: data,
});

export const delOneBookmark = (id: number) => ({
  type: removeOneBookmark,
  payload: id,
});
export const delAllBookmark = () => ({
  type: removeAllBookmarks,
});
export const readBookmarks = () => ({ type: readAllBookmarks });
export const clearBookmarks = () => ({ type: clearBookmark });
