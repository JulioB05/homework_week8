import { Data, Result } from '../../Interfaces/apiCharacters';
import {
  readAllDataCharacter,
  readFiltersSelectCharacters,
  readCurrentPageCharacters,
} from '../Types';

export type actionTypes =
  | { type: 'readAllDataCharacter'; payload: Data }
  | {
      type: 'readFiltersSelectCharacters';
      payload: { type: string; id: string };
    }
  | { type: 'readCurrentPageCharacters'; payload: number };

export const saveAllDataCharacter = (data: Data) => ({
  type: readAllDataCharacter,
  payload: data,
});
export const addFiltersCharacters = (data: { type: string; id: string }) => ({
  type: readFiltersSelectCharacters,
  payload: data,
});
export const changeCurrentPage = (data: number) => ({
  type: readCurrentPageCharacters,
  payload: data,
});
