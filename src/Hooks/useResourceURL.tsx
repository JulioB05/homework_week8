import React, { useEffect } from 'react';

const useResourceURL = () => {
  type resource = 'characters' | 'comics' | 'stories';

  const getResourceCharacterURL = (
    type: resource,
    page: number,
    search: string | null,
    filterId: string | undefined,
    typeFilter: string | undefined
  ) => {
    const numberLimit = 20;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;

    if (search !== null && search !== '') {
      return `https://gateway.marvel.com:443/v1/public/${type}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}&nameStartsWith=${search}`;
    } else if (
      typeFilter !== undefined &&
      typeFilter !== '' &&
      filterId !== 'Select a comic' &&
      filterId !== 'Select a stories'
    ) {
      return `https://gateway.marvel.com:443/v1/public/${typeFilter}/${filterId}/characters?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}`;
    }

    return `https://gateway.marvel.com:443/v1/public/${type}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}`;
  };

  const getResourceComicsURL = (
    type: resource,
    page: number,
    search: string | null,
    filterId: string | undefined,
    typeFilter: string | undefined
  ) => {
    const numberLimit = 20;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;

    if (search !== null && search !== '') {
      return `https://gateway.marvel.com:443/v1/public/${type}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}&titleStartsWith=${search}`;
    } else if (
      typeFilter !== undefined &&
      typeFilter !== '' &&
      filterId !== 'Select a format' &&
      filterId !== 'Select a title'
    ) {
      return `https://gateway.marvel.com:443/v1/public/comics?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=0&${typeFilter}=${filterId}`;
    }
    return `https://gateway.marvel.com:443/v1/public/${type}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}`;
  };

  const getResourceStoriesURL = (
    type: resource,
    page: number,
    filterId: string | undefined,
    typeFilter: string | undefined
  ) => {
    const numberLimit = 20;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;
    if (
      typeFilter !== undefined &&
      typeFilter !== '' &&
      filterId !== 'Select a characters'
    ) {
      return `https://gateway.marvel.com:443/v1/public/${typeFilter}/${filterId}/stories?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}`;
    }

    return `https://gateway.marvel.com:443/v1/public/${type}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=${indexOfFirstGame}`;
  };

  const getResourceDetailsURL = (
    type: string | undefined,
    id: number | string | undefined
  ) => {
    return `https://gateway.marvel.com:443/v1/public/${type}/${id}?apikey=f5bf1060852e4bb6f0712efc5b34341b&hash=95dce2ae47d9973879df5a0045d71134&ts=1&offset=0`;
  };

  return {
    getResourceCharacterURL,
    getResourceComicsURL,
    getResourceStoriesURL,
    getResourceDetailsURL,
  };
};
export default useResourceURL;
