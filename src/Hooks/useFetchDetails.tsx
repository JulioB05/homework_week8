import { useEffect, useState } from 'react';
import axios from 'axios';
import { Characters, Stories } from '../Interfaces/apiComics';
import { Comics } from '../Interfaces/apiCharacters';

const useFetchDetails = (url: string, typeFromPage: string | undefined) => {
  const [id, setId] = useState('');
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [image, setImage] = useState('');
  const [comics, setComics] = useState(null);
  const [characters, setCharacters] = useState(null);
  const [stories, setStories] = useState(null);

  const [data, setData] = useState(Object);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  interface dataDetailsTypes {
    id: string;
    title: string;
    body: string;
    image: string;
    comics?: Comics | null;
    characters?: Characters | null;
    stories?: Stories | null;
  }
  const dataDetails: dataDetailsTypes = {
    id: id,
    title: title,
    body: body,
    image: image,
    comics: comics,
    characters: characters,
    stories: stories,
  };

  useEffect(() => {
    setLoading(true);
    axios
      .get(url)
      .then((response) => {
        setData(response.data);

        let img = `${response.data.data.results[0].thumbnail.path}.${response.data.data.results[0].thumbnail.extension}`;
        setId(response.data.data.results[0].id);
        setBody(response.data.data.results[0].description);
        setImage(img);

        if (typeFromPage === 'comics') {
          setTitle(response.data.data.results[0].title);
          setCharacters(response.data.data.results[0].characters);
          setStories(response.data.data.results[0].stories);
        } else if (typeFromPage === 'characters') {
          setTitle(response.data.data.results[0].name);
          setComics(response.data.data.results[0].comics);
          setStories(response.data.data.results[0].stories);
        } else {
          setImage(
            `https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png`
          );
          setComics(response.data.data.results[0].comics);
          setCharacters(response.data.data.results[0].characters);
        }
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url]);

  const refetch = () => {
    setLoading(true);
    axios
      .get(url)
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return { dataDetails, data, loading, error, refetch };
};

export default useFetchDetails;

function setLoading(arg0: boolean) {
  throw new Error('Function not implemented.');
}
