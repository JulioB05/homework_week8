import { useEffect, useState } from 'react';
import FetchDataStories from '../../Components/FetchData/FetchDataStories';
import Header from '../../Components/Header';
import { useSearchParams } from 'react-router-dom';
import './style.scss';
import useResourceURL from '../../Hooks/useResourceURL';
import useFetch from '../../Hooks/useFetch';
import { saveAllDataStories } from '../../Redux/Actions/apiStoriesActions';
import Loader from '../../Components/Loader';
import { useDispatch } from 'react-redux';
import Footer from '../../Components/Footer';
import FilterBar from '../../Components/FilterBar';
import FilterCharacter from '../../Utils/filtersStories';

const Stories = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [filterSelect, setFilterSelect] = useState({ type: '', id: '' });
  const { getResourceStoriesURL } = useResourceURL();
  const dispatch = useDispatch();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceStoriesURL(
    'stories',
    currentPage,
    filterSelect.id,
    filterSelect.type
  );

  const { data, loading, error, refetch } = useFetch(url);

  dispatch(saveAllDataStories(data?.data));
  const sizeOfData = data?.data?.total;

  const content = () => {};

  return (
    <div className="body-stories">
      <Header />
      <FilterBar setFilterSelect={setFilterSelect} filter={FilterCharacter} />
      <FetchDataStories params={params} apiStatus={loading} />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
      />
    </div>
  );
};

export default Stories;
