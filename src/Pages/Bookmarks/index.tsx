import { useEffect } from 'react';
import Card from '../../Components/Card';
import FilterBar from '../../Components/FilterBar';
import Header from '../../Components/Header';
import { store } from '../../Redux/Store';
import './style.scss';

const Bookmarks = () => {
  const state = store.getState();
  interface bookmarkType {
    id: number;
    name: string;
    type: string;
    img: string;
  }

  const { bookmarks } = state.bookmarks;

  const data = bookmarks as bookmarkType[];

  useEffect(() => {}, [bookmarks]);

  return (
    <div className="bookmarks-page">
      <Header></Header>
      <FilterBar filterOff={true} bookmarks={true}></FilterBar>
      <div className="bookmarks">
        <h1 className="title-products">Bookmarks</h1>
        <div className="cards">
          {data?.map((item) => {
            const img = item.img
              ? item.img
              : 'https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png';
            return (
              <Card
                key={item.id}
                id={item.id}
                title={item.name}
                imgUrl={img}
                body={''}
                date={''}
                category={item.type}
                content={true}
              ></Card>
            );
          })}
        </div>
      </div>{' '}
    </div>
  );
};
export default Bookmarks;
