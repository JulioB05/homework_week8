import { useEffect, useState } from 'react';
import FetchDataComics from '../../Components/FetchData/FetchDataComics';
import Header from '../../Components/Header';
import SearchBox from '../../Components/SearchBox';
import { useSearchParams } from 'react-router-dom';
import './style.scss';
import useResourceURL from '../../Hooks/useResourceURL';
import useFetch from '../../Hooks/useFetch';
import {
  saveAllDataComics,
  sizeOfDataComics,
} from '../../Redux/Actions/apiComicsActions';
import Loader from '../../Components/Loader';
import { useDispatch } from 'react-redux';
import Footer from '../../Components/Footer';
import { FiltersFormat, FiltersTitle } from '../../Utils/filtersComics';

const Comics = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [filterSelect, setFilterSelect] = useState({ type: '', id: '' });
  const { getResourceComicsURL } = useResourceURL();
  const dispatch = useDispatch();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceComicsURL(
    'comics',
    currentPage,
    params.name,
    filterSelect.id,
    filterSelect.type
  );

  const { data, loading, error, refetch } = useFetch(url);

  dispatch(saveAllDataComics(data?.data));
  const sizeOfData = data?.data?.total;

  const content = () => {};

  return (
    <div className="body-comics">
      <Header />
      <SearchBox
        setSearchParams={setSearchParams}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setFilterSelect={setFilterSelect}
        filter={FiltersFormat}
        filter2={FiltersTitle}
      />
      <FetchDataComics params={params} apiStatus={loading} />

      <Loader apiState={loading} />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
      />
    </div>
  );
};

export default Comics;
