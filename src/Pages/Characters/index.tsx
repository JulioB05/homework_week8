import { useEffect, useState } from 'react';
import FetchDataCharacters from '../../Components/FetchData/FetchDataCharacters';
import Header from '../../Components/Header';
import SearchBox from '../../Components/SearchBox';
import { useSearchParams } from 'react-router-dom';
import './style.scss';
import useResourceURL from '../../Hooks/useResourceURL';
import useFetch from '../../Hooks/useFetch';
import { saveAllDataCharacter } from '../../Redux/Actions/apiCharacterActions';
import Loader from '../../Components/Loader';
import { useDispatch } from 'react-redux';
import Footer from '../../Components/Footer';
import { FiltersComics, FiltersStories } from '../../Utils/filtersCharacters';

const Characters = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const [currentPage, setCurrentPage] = useState(1);
  const [filterSelect, setFilterSelect] = useState({ type: '', id: '' });

  const { getResourceCharacterURL } = useResourceURL();
  const dispatch = useDispatch();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceCharacterURL(
    'characters',
    currentPage,
    params.name,
    filterSelect.id,
    filterSelect.type
  );
  const { data, loading, error, refetch } = useFetch(url);
  dispatch(saveAllDataCharacter(data?.data));
  const sizeOfData = data?.data?.total;

  return (
    <div className="body-characters">
      <Header />
      <SearchBox
        setSearchParams={setSearchParams}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        filter={FiltersComics}
        filter2={FiltersStories}
        setFilterSelect={setFilterSelect}
      />
      <FetchDataCharacters params={params} apiStatus={loading} />
      <Loader apiState={loading} />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
      />
    </div>
  );
};

export default Characters;
