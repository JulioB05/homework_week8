import './style.scss';
import { Comics } from '../../Interfaces/apiCharacters';
import { Characters, Stories } from '../../Interfaces/apiComics';
interface DetailsCardProps {
  data: {
    id: string;
    title: string;
    body: string;
    image: string;
    comics?: Comics | null;
    characters?: Characters | null;
    stories?: Stories | null;
  };
}

const DetailsCard = (props: DetailsCardProps) => {
  const { data } = props;

  return (
    <div className="details-page">
      <div className="container">
        <div className="container-1">
          <div className="container-img">
            <img src={data.image} alt="" />
          </div>
          <div className="container-info">
            <h1>{data.title}</h1>
            <h2>{data.body}</h2>
          </div>
        </div>
        <div className="container-2">
          {data.comics ? (
            <div className="container-data1">
              <h2>COMICS</h2>
              {data.comics?.items.map((comic) => {
                return <p key={comic.name}>{comic.name}</p>;
              })}
            </div>
          ) : (
            ''
          )}
          {data.stories ? (
            <div className="container-data1">
              <h2>STORIES</h2>
              {data.stories?.items.map((story) => {
                return <p key={story.name}>{story.name}</p>;
              })}
            </div>
          ) : (
            ''
          )}
          {data.characters ? (
            <div className="container-data1">
              <h2>CHARACTERS</h2>
              {data.characters?.items.map((character) => {
                return <p key={character.name}>{character.name}</p>;
              })}
            </div>
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  );
};

export default DetailsCard;
