import React, { useState, useMemo, useReducer, useEffect } from 'react';
import { store } from '../../Redux/Store';

// import {
//   initialStateValues,
//   reducerPaginator,
// } from '../../reducerComponents/ReducerPaginator';
import './style.scss';

interface PaginationProps {
  currentPage: number;
  setCurrentPage: Function;
  setSearchParams: Function;
  sizeOfData: number;
}

const Paginator = (props: PaginationProps) => {
  const { currentPage, setCurrentPage, setSearchParams, sizeOfData } = props;
  const [clicked, setClicked] = useState(true);
  const [slicePages, setSlicePages] = useState(1);

  const maxNumberPage = useMemo(() => {
    return Math.ceil(sizeOfData / 20);
  }, [sizeOfData]);

  const arrayPages = [];
  for (let i = 0; i < maxNumberPage; i++) {
    const a = {
      id: i + 1,
      numberPage: i + 1,
    };

    arrayPages.push(a);
  }

  const indexOfLastPage = slicePages * 8;
  const indexOfFirstPage = indexOfLastPage - 8;
  const currentButtonsPages = arrayPages.slice(
    indexOfFirstPage,
    indexOfLastPage
  );

  useEffect(() => {
    if (currentPage > indexOfLastPage) {
      setSlicePages(slicePages + 1);
    } else if (currentPage < indexOfFirstPage + 1) {
      setSlicePages(slicePages - 1);
    }
  }, [currentPage]);

  const HandleChange = (page: number) => {
    setCurrentPage(page);
    setSearchParams({ page: page });
  };

  const nextPage = () => {
    const nextPage = currentPage + 1;
    if (currentPage < maxNumberPage) {
      setCurrentPage(nextPage);
      setClicked(true);
      setSearchParams({ page: nextPage });
    }
  };

  const prevPage = () => {
    const prevPage = currentPage - 1;
    if (currentPage > 1) {
      setCurrentPage(prevPage);
      setClicked(true);
      setSearchParams({ page: prevPage });
    }
  };

  return (
    <div>
      <div className="pagination">
        <button
          className="BtnNextPrev"
          onClick={prevPage}
          disabled={currentPage === 1}
        >
          PREVIOUS
        </button>

        {arrayPages.length
          ? currentButtonsPages?.map((page) => {
              const { id, numberPage } = page;
              return (
                <button
                  key={id}
                  onClick={() => HandleChange(numberPage)}
                  className={`pageNumberList ${
                    currentPage === id && clicked ? ' active' : ''
                  }`}
                >
                  {numberPage}
                </button>
              );
            })
          : null}

        <button
          className="BtnNextPrev"
          onClick={nextPage}
          disabled={currentPage === maxNumberPage}
        >
          NEXT
        </button>
      </div>
    </div>
  );
};

export default Paginator;
