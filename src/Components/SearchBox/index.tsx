import React, { ChangeEvent, useRef } from 'react';
import Filter from '../Filter';
import './style.scss';

interface SearchBoxProps {
  setSearchParams: Function;
  currentPage: number;
  setCurrentPage: Function;
  filter?: { type: string; data: { id: number | string; name: string }[] };
  filter2?: { type: string; data: { id: number | string; name: string }[] };
  setFilterSelect: Function;
}

const SearchBox = (props: SearchBoxProps) => {
  const {
    setSearchParams,
    currentPage,
    setCurrentPage,
    filter,
    filter2,
    setFilterSelect,
  } = props;

  const debounceRef = useRef<NodeJS.Timeout>();

  const onQueryChanged = (e: ChangeEvent<HTMLInputElement>) => {
    if (debounceRef.current) {
      clearTimeout(debounceRef.current);
    }

    debounceRef.current = setTimeout(() => {
      setSearchParams({ name: e.target.value, page: currentPage });
      setCurrentPage(1);
    }, 500);
  };

  return (
    <div>
      <form className="search-form">
        <div className="form-group">
          <label className="title-search">SEARCH: </label>
          <input
            type="text"
            className="form-control"
            placeholder="Write your search"
            onChange={onQueryChanged}
          ></input>
          <div className="filters">
            <Filter filter={filter} setFilterSelect={setFilterSelect}></Filter>

            <Filter filter={filter2} setFilterSelect={setFilterSelect}></Filter>
          </div>
        </div>
      </form>
    </div>
  );
};

export default SearchBox;
