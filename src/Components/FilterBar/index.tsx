import Filter from '../Filter';
import './style.scss';
import { useDispatch } from 'react-redux';
import { delAllBookmark } from '../../Redux/Actions/bookmarkActions';
interface FilterBarProps {
  setFilterSelect?: Function;
  filter?: { type: string; data: { id: number; name: string }[] };
  filterOff?: boolean;
  bookmarks?: boolean;
}
const FilterBar = (props: FilterBarProps) => {
  const { setFilterSelect, filter, filterOff, bookmarks } = props;
  const dispatch = useDispatch();
  const deleteBookmarks = () => {
    dispatch(delAllBookmark());
  };
  return (
    <div>
      <form className="search-form">
        <div className="form-group">
          <div className="filters">
            {filterOff ? (
              ''
            ) : (
              <Filter
                setFilterSelect={setFilterSelect}
                filter={filter}
              ></Filter>
            )}

            {bookmarks ? (
              <button onClick={deleteBookmarks}>Delete all bookmarks</button>
            ) : (
              ''
            )}
          </div>
        </div>
      </form>
    </div>
  );
};

export default FilterBar;
