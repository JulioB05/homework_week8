import React, { ChangeEvent } from 'react';

import './style.scss';

interface FilterProps {
  filter?: { type: string; data: { id: number | string; name: string }[] };
  setFilterSelect: Function | undefined;
}

const Filter = (props: FilterProps) => {
  const { filter, setFilterSelect } = props;

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const filterObj = { type: filter?.type, id: e.target.value };
    if (typeof setFilterSelect === 'function') {
      setFilterSelect(filterObj);
    }
  };
  return (
    <div className="filters">
      <label className="label"> {filter?.type.toUpperCase()} </label>
      <select onChange={(e) => handleChange(e)} className="selects">
        <option>Select a {filter?.type}</option>
        {filter?.data.map((filter) => {
          return (
            <option key={filter.id} value={filter.id}>
              {filter.name}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Filter;
