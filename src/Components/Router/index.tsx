import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Bookmarks from '../../Pages/Bookmarks';
import Characters from '../../Pages/Characters';
import Comics from '../../Pages/Comics';
import Details from '../../Pages/Details';
import Home from '../../Pages/Home';
import Stories from '../../Pages/Stories';
import PathRoutes from '../../Utils/enumPath';

interface RouterProps {}

const Router: React.FunctionComponent<RouterProps> = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={PathRoutes.Home} element={<Home />} />
        <Route path={PathRoutes.Characters} element={<Characters />} />
        <Route path={PathRoutes.Comics} element={<Comics />} />
        <Route path={PathRoutes.Stories} element={<Stories />} />
        <Route path={PathRoutes.Bookmarks} element={<Bookmarks />} />

        <Route path={PathRoutes.Details}>
          <Route index element={<Details />}></Route>
          <Route path=":type/:id" element={<Details />}></Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
