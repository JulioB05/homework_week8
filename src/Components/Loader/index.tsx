/* eslint-disable react/prop-types */
import React from 'react';
import './style.scss';

interface LoaderProps {
  apiState: boolean;
}

const Loader = (props: LoaderProps) => {
  const { apiState } = props;
  return (
    <div className={apiState ? 'loading' : 'Arrived'}>
      LOADING...
      <span className="loaderStyle"> </span>
    </div>
  );
};

export default Loader;
