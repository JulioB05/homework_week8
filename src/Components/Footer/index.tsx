import React from 'react';
import Paginator from '../Paginator';
import './style.scss';

interface FooterProps {
  currentPage: number;
  setCurrentPage: Function;
  setSearchParams: Function;
  sizeOfData: number;
}

const Footer: React.FunctionComponent<FooterProps> = ({
  currentPage,
  setCurrentPage,
  setSearchParams,
  sizeOfData,
}) => {
  return (
    <div>
      <Paginator
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
      ></Paginator>
    </div>
  );
};

export default Footer;
