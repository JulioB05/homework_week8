import React from 'react';
import '../style.scss';

interface BurguerButtonProps {
  clicked: boolean;
  handleClick: () => void;
}

const BurguerButton: React.FunctionComponent<BurguerButtonProps> = ({
  clicked,
  handleClick,
}) => {
  return (
    <div
      onClick={handleClick}
      className={`icon nav-icon-5 ${clicked ? 'open' : ''}`}
    >
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};
export default BurguerButton;
