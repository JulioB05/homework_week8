/* eslint-disable no-restricted-globals */

import React, { useState } from 'react';
import './style.scss';
import BurguerButton from './BurguerButton';
import { Link } from 'react-router-dom';
import PathRoutes from '../../Utils/enumPath';
import marvelLogo from '../../Images/marvel-logo.png';
import shield from '../../Images/shield.png';
import hydra from '../../Images/hydra.png';
import wanda from '../../Images/wanda.png';
import spider from '../../Images/spider.png';

interface HeaderProps {}

const Header: React.FunctionComponent<HeaderProps> = () => {
  const [clicked, setClicked] = useState(false);

  const userData = localStorage.getItem('userData');
  let userDataJson;
  if (userData) {
    userDataJson = JSON.parse(userData);
  }

  const logoutUser = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('userData');
    location.href = '/';
  };

  return (
    <div>
      <header>
        <div className="navContainer">
          <Link to={PathRoutes.Home} className="logo-link">
            <img src={marvelLogo} alt="" className="logo" />
          </Link>

          <img src={shield} alt="shield" className="shield" />
          <img src={hydra} alt="shield" className="hydra" />
          <img src={wanda} alt="shield" className="wanda" />
          <img src={spider} alt="shield" className="spider" />

          <div className={`links ${clicked ? 'active' : ''}`}>
            <Link to={PathRoutes.Home} className="link">
              Home
            </Link>

            <Link to={PathRoutes.Characters} className="link">
              Characters
            </Link>
            <Link to={PathRoutes.Comics} className="link">
              Comics
            </Link>
            <Link to={PathRoutes.Stories} className="link">
              Stories
            </Link>
            <Link to={PathRoutes.Bookmarks} className="link">
              Bookmarks
            </Link>
          </div>

          <div className="burguer">
            <BurguerButton
              handleClick={() => {
                setClicked(!clicked);
              }}
              clicked={clicked}
            ></BurguerButton>
          </div>

          <div className={`BgDiv initial ${clicked ? ' active' : ''}`}></div>
        </div>
      </header>
    </div>
  );
};

export default Header;
