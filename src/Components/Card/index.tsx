/* eslint-disable no-restricted-globals */
import './style.scss';
import star from '../../Images/star.png';
import { Link } from 'react-router-dom';
import PathRoutes from '../../Utils/enumPath';
import { store } from '../../Redux/Store';
import { useDispatch } from 'react-redux';
import {
  addBookmark,
  delOneBookmark,
} from '../../Redux/Actions/bookmarkActions';

interface CardProps {
  id: number;
  title: string;
  imgUrl: string;
  body: string;
  date: string;
  category: string;
  content?: boolean;
}

const Card = (props: CardProps) => {
  const { id, title, imgUrl, body, category, content } = props;
  const dispatch = useDispatch();
  const state = store.getState();

  const { bookmarks } = state.bookmarks;
  interface bookmarkType {
    id: number;
    name: string;
    type: string;
    img: string;
  }
  const bookmarksData = bookmarks as bookmarkType[];

  const functionClick = () => {
    const data = { id: id, name: title, type: category, img: imgUrl };
    const existData = bookmarksData?.filter((el) => el.id === id);

    if (existData.length < 1) {
      dispatch(addBookmark(data));
    }
  };

  const deleteBookmark = () => {
    dispatch(delOneBookmark(id));
  };

  return (
    <div className="card">
      <Link to={`${PathRoutes.Details}/${category}/${id}`} className="link-det">
        <div className="container">
          <img
            className="imageNew"
            src={imgUrl ? imgUrl : 'https://i.gifer.com/3sqI.gif'}
            alt={title}
          ></img>
        </div>

        <div className="details">
          <h3>{title}</h3>
          <p> {body} </p>
          <p> Type: {category}</p>
        </div>
      </Link>

      {content ? (
        <Link to={PathRoutes.Bookmarks}>
          <button onClick={deleteBookmark} className="button-bookmark">
            Delete bookmark
          </button>
        </Link>
      ) : (
        ''
      )}

      <div className="icon-shield">
        <Link to={`/${category}`}>
          <img
            src={star}
            alt=""
            className="img-shield"
            onClick={functionClick}
          />
        </Link>
      </div>
    </div>
  );
};

export default Card;
