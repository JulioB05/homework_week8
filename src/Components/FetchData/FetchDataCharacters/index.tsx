import Card from '../../Card';
import { store } from '../../../Redux/Store';
import './style.scss';
import Loader from '../../Loader';

interface FetchDataCharactersProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
}

const FetchDataCharacters = (props: FetchDataCharactersProps) => {
  const { params, apiStatus } = props;

  const state = store.getState();
  const { dataBaseCharacter } = state.characterReducer;

  return (
    <div className="body-fetch-data-characters">
      <h1 className="title-products">Characters</h1>
      <div className="cards">
        {dataBaseCharacter ? (
          dataBaseCharacter.results.map(
            (character: {
              modified: string;
              description: string;
              thumbnail: any;
              name: string;
              id: number;
            }) => {
              let img = `${character.thumbnail.path}.${character.thumbnail.extension}`;
              return (
                <Card
                  key={character.id}
                  id={character.id}
                  title={character.name}
                  imgUrl={img}
                  body={character.description}
                  date={''}
                  category={'characters'}
                ></Card>
              );
            }
          )
        ) : (
          <>
            {apiStatus ? (
              <Loader apiState={apiStatus} />
            ) : (
              <p className="not-found">Not found: {params.name}</p>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default FetchDataCharacters;
