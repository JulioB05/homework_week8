import Card from '../../Card';
import { store } from '../../../Redux/Store';
import './style.scss';
import Loader from '../../Loader';

interface FetchDataComicsProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
}

const FetchDataComics = (props: FetchDataComicsProps) => {
  const { params, apiStatus } = props;

  const state = store.getState();
  const { dataBaseComics } = state.comicsReducer;

  return (
    <div className="body-fetch-data-comics">
      <h1 className="title-products">Comics</h1>
      <div className="cards-comics">
        {dataBaseComics ? (
          dataBaseComics.results.map(
            (comic: {
              modified: string;
              format: string;
              id: number;
              thumbnail: any;
              title: string;
            }) => {
              let img = `${comic.thumbnail.path}.${comic.thumbnail.extension}`;
              return (
                <Card
                  key={comic.id}
                  id={comic.id}
                  title={comic.title}
                  imgUrl={img}
                  body={comic.format}
                  date={comic.modified}
                  category={'comics'}
                ></Card>
              );
            }
          )
        ) : (
          <>
            {apiStatus ? (
              <Loader apiState={apiStatus} />
            ) : (
              <p className="not-found">Not found: {params.name}</p>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default FetchDataComics;
