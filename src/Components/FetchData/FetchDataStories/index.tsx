import Card from '../../Card';
import { store } from '../../../Redux/Store';
import './style.scss';
import Loader from '../../Loader';

interface FetchDataStoriesProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
}

const FetchDataStories = (props: FetchDataStoriesProps) => {
  const { params, apiStatus } = props;

  const state = store.getState();
  const { dataBaseStories } = state.storiesReducer;

  return (
    <div className="body-fetch-data-stories">
      <h1 className="title-products">Stories</h1>
      <div className="cards-stories">
        {dataBaseStories ? (
          dataBaseStories.results.map(
            (story: {
              modified: string;
              type: string;
              id: number;
              title: string;
            }) => {
              let img = `https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png`;
              return (
                <Card
                  key={story.id}
                  id={story.id}
                  title={story.title}
                  imgUrl={img}
                  body={story.type}
                  date={story.modified}
                  category={'stories'}
                ></Card>
              );
            }
          )
        ) : (
          <Loader apiState={apiStatus}></Loader>
        )}
      </div>
    </div>
  );
};

export default FetchDataStories;
